# Operation Manual: PrintrBot 3D Printer

<img src="../../qrcodes/printrbot-3d-printer.png" style="background-color: white; display:block"/>

This document will outline the process to use the Printrbot 3D Printer.

## Training Requirements

```
NOTE: THIS EQUIPMENT REQUIRES TRAINING VIA ON IN-PERSON EVENT.
SIGN UP FOR A TRAINING ON THE [EVENTS PORTION OF PROTOTYPEPGH.COM](https://prototypepgh.com/events)
```

---

## Operations

### Walkthrough Video

```
----Coming Soon -----
For a Video Of this Process
```

### Before You Start

\*tips, tricks or other things that will make the steps ahead easier.

### Using the Equipment

Steps For Use:

1. Make sure the Printrbot power cable is plugged into the electrical outlet or power strip (if there is power running to the Printrbot, a blue light will appear on the power cord)
2. Make sure the Printrbot is plugged into the laptop via USB cable.
3. Open up CURA 3D printing software on the black laptop.
4. Do a test print (I usually print a cat ring which is saved onto the desktop or you can find a simple print from Thingiverse.com)
5. To do a test print, open the cat ring file (or whatever file you want as long as it is small and in `.stl` format) and then click on th edrop down menu in the bottom right corner of CURA that says "save file" and then choose "print file" and make sure "Printrbot Simple" is selected as the printer.
6. Once you've clicked "print file" a progress bar should appear as well as the temperature of the Printrbot. Both should incrementally progress until the red tip of the Printrbot because hot to the touch (it will burn you so be careful!) and the Printrbot will start printing.
7. Only use the PLA filament on this printer ABS Filament will not adhere to the printer bed becuase the bed is not heated (ABS requires a headed printer bed which we do not have).
8. To feed the PLA filament into th eprinter, wait for the red nozzle to heat up and then feed the filament into the metal opening abobe the extruder while pressing down on the clamp on the left.
9. When your print is done, use the plastic spatula (not the metal one becuase it will scratch the bed) to remove the print.
10. If you need to change filament, this can only be done when the nozzle is hot.

### Other Useful Resources

Designing an object can be done with a variety of software tools:

- [TinkerCAD](https://www.tinkercad.com/) - A "Free for Everyone" software from AutoDesk that allows you to build beginner objects right in the browser!
- [Fusion360](https://www.autodesk.com/products/fusion-360/overview?term=1-YEAR&tab=subscription) - Another product by AutoDesk but the professional (and pricey) product.
- [Blender](https://www.blender.org/) - An open source artistry tool that can not only do modeling but also other things related to animation, game design and video editing. (Seriously powerful stuff but might feel overwhelming)
- [SolidWorks](https://www.solidworks.com/) - A competitor to AutoDesk, this is another professional grade software with likely $$$ Pricing.

You can also download pre-built models from websites like [thingiverse.com](thingiverse.com), and [Printables.com](Printables.com). You'll want files that end in `.stl` or `3mf`.

Once you have a 3D Object, you may need to slice it. You can use programs like Cura, Slic3r or Prusa Slicer

### Edit this document

This document is hosted on gitlab.com and is currently being maintained by (INSERT NAME HERE) and/or the Operations Committee at Large.

If you think this document can benefit from improvement, submit a PULL REQUEST or contact the maintainer.

Other contributors include:

- (if you make an edit or suggestion put your name here to get credit!)
